export function addErrors(document, errors) {
  if (!errors) return;
  var entries = Object.keys(errors)
    .filter((key) => errors[key] != null)
    .filter((key) => errors[key] == "");
  //   console.log(
  //     entries.map((entry) => {
  //       return { entry, value: errors[entry] };
  //     })
  //   );
  if (entries.length > 0) {
    entries.forEach((name) => {
      let inputNode = document.querySelector(`form [name="${name}"]`);

      if (inputNode) {
        inputNode.classList.add("input-error");
        inputNode.onchange = (e) => inputNode.classList.remove("input-error");
        inputNode.addEventListener("change", () => {
          inputNode.classList.remove("input-error");
        });
      }
    });
  }
}

export function clearErrors(document, errors, name) {
  if (!errors) return;
  if (name) return (errors[name] = null);
  var entries = Object.keys(errors);
  if (entries.length > 0) {
    entries.forEach((name) => {
      let inputNode = document.querySelector(`form [name="${name}"]`);
      if (inputNode) {
        inputNode.classList.remove("input-error");
      }
    });
  }
}

export function numberKeyValidator(event) {
  let prevent = false;
  if (event.key === "e") prevent = true;
  if (event.key === ".") prevent = true;
  if (event.key === ",") prevent = true;
  prevent && event.preventDefault();
}
