export const FormError = ({ children, errorData= {}, name }) => {
  if (errorData[name]?.length <= 0) return <></>;
  // console.log(name,errorData);
  return <span className={`form-error`}>{errorData[name]}</span>;
};
