import React, { useEffect, useRef, useState } from "react";
import { Button, Modal } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import {
  useResendOTPWithdrawMutation,
  useVerifyWithdrawMutation,
} from "../../redux/api/otpApi"; // Import hai API mới
import { LoadingButton } from "@mui/lab";
import { useTheme } from '@mui/material/styles';

const OTPWithdrawPage = ({ withdrawId, onClose }) => {
  const navigate = useNavigate();
  const [otp, setOtp] = useState([]);
  const inputRefs = useRef([]);
  const [modalShow, setModalShow] = useState(true);

  const [verifyOTP] = useVerifyWithdrawMutation(); // Sử dụng API mới để xác minh OTP
  const [resendOTP] = useResendOTPWithdrawMutation(); // Sử dụng API mới để gửi lại OTP
  const [isLoading, setIsLoading] = useState(false);
  const [isLoadingResent, setIsLoadingResent] = useState(false);
  const theme = useTheme();
  useEffect(() => {
    // Hiển thị modal khi component được render
    setModalShow(true);
  }, []);

  const handleOTPChange = (e, index) => {
    const newOTP = [...otp];
    newOTP[index] = e.target.value;
    setOtp(newOTP);

    if (e.target.value.length === 1 && index < 5) {
      inputRefs.current[index + 1].focus();
    }
  };

  const handleClearOTP = () => {
    setOtp([]);
    inputRefs.current[0].focus();
  };
  const handlePaste = (e) => {
    e.preventDefault();
    const pastedValue = e.clipboardData.getData("Text");
    setOtp(pastedValue);
    inputRefs.current[5].focus();
  };
  const handleConfirmOrder = async () => {
    try {
      setIsLoading(true);
      const response = await verifyOTP({
        otp: Array.isArray(otp) ? otp.join("") : otp,

        withdrawId: withdrawId,
      });

      if (response?.data?.status === "OK") {
        // Đóng modal khi nhập OTP thành công
        setModalShow(false);
        Swal.fire({
          icon: "success",
          title: "OTP confirmed successfully!",
        });
        // Load lại trang khi OTP được xác nhận thành công
        window.location.reload();
        setIsLoading(false);
      } else {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          // text: "Incorrect OTP. Please try again.",
          text: response.error.data.message,
        });
        setIsLoading(false);
      }
    } catch (error) {
      setIsLoading(false);
      console.error("Error verifying OTP:", error);
    }
  };

  const handleResendOTP = async () => {
    try {
      setIsLoadingResent(true);
      await resendOTP({ withdrawId: withdrawId });
      Swal.fire({
        icon: "success",
        title: "OTP resent successfully",
      });
      setIsLoadingResent(false);
    } catch (error) {
      console.error("Error resending OTP:", error);
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: "Failed to resend OTP. Please try again.",
      });
      setIsLoadingResent(false);
    }
  };

  return (
    <Modal show={modalShow} onHide={onClose}>
      <Modal.Header closeButton>
        <Modal.Title>Enter OTP</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <p style={{ marginBottom: "30px" }}>
          Please check your email to receive the OTP code
        </p>
        <div className="d-flex justify-content-between">
          {[...Array(6)].map((_, index) => (
            <input
              ref={(el) => (inputRefs.current[index] = el)}
              key={index}
              type="text"
              maxLength="1"
              className="otp-input"
              value={otp[index] || ""}
              onPaste={handlePaste}
              onChange={(e) => handleOTPChange(e, index)}
              onFocus={(e) => e.target.select()}
            />
          ))}
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="danger" onClick={handleClearOTP}>
          Clear
        </Button>
        {/* <Button variant="info" onClick={handleResendOTP}> */}
        <LoadingButton variant="outlined" onClick={handleResendOTP} type='submit' loading={isLoadingResent}
          sx={{
            backgroundColor: theme.palette.info.main,
            marginRight: '5px',
            color: 'white',
            '&:hover': {
              backgroundColor: theme.palette.info.dark,
            },
          }}
          
          >
          Resend OTP
        </LoadingButton>{"     "}
        <LoadingButton variant="outlined" onClick={handleConfirmOrder} type='submit' loading={isLoading}
          sx={{
            backgroundColor: 'orange',
            color: 'white',
            '&:hover': {
              backgroundColor: 'darkorange',
            },
          }}
          >
          Confirm OTP
        </LoadingButton>
      </Modal.Footer>
    </Modal>
  );
};

export default OTPWithdrawPage;
