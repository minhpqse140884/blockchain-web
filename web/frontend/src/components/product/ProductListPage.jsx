import React, { useEffect } from "react";
import toast from "react-hot-toast";
import { useSelector } from "react-redux";
import { useNavigate, useSearchParams } from "react-router-dom";
import DiscountCard from "../../helpers/components/discount-card";
import {
  useAddToMyDiscountMutation,
  useGetAllRandomDiscountQuery,
  useGetMyDiscountQuery,
} from "../../redux/api/discountApi";
import { useGetProductsQuery } from "../../redux/api/productsApi";
import CustomPagination from "../layout/CustomPagination";
import Filters from "../layout/Filters";
import Loader from "../layout/Loader";
import MetaData from "../layout/MetaData";
import Search from "../layout/Search";
import ProductItem from "../product/ProductItem";

const ProductListPage = () => {
  const [searchParams] = useSearchParams();
  const page = searchParams.get("page");
  const keyword = searchParams.get("keyword");
  const min = searchParams.get("min");
  const max = searchParams.get("max");
  const asc = searchParams.get("asc");
  const category = searchParams.getAll("category");
  const reviews = searchParams.getAll("reviews");

  const { isAuthenticated, roles } = useSelector((s) => s.auth);
  const navigate = useNavigate();

  const params = { page, keyword, asc };
  // console.log(isNaN(min) ? "" : min)
  min !== null && (params.min = isNaN(min) ? "" : min);
  max !== null && (params.max = isNaN(min) ? "" : max);
  category !== null && (params.category = category);
  reviews !== null && (params.reviews = reviews);
  // console.log(params)
  const { data, isLoading, error, isError } = useGetProductsQuery(params);
  const discountResult = useGetAllRandomDiscountQuery(4);
  const userDiscountResult = useGetMyDiscountQuery();
  const [applyDiscount, applyDiscountResult] = useAddToMyDiscountMutation();

  useEffect(() => {
    if (isError) {
      toast.error(error?.data?.message);
    }
    if (applyDiscountResult.isError) {
      toast.error(applyDiscountResult.error.data.message);
    }
    if (applyDiscountResult.isSuccess) {
      toast.success("Discount Saved!");
    }
  }, [isError, applyDiscountResult.isSuccess, applyDiscountResult.isError]);
  const columnSize = keyword ? 4 : 4;
  const products = data?.data;
  if (isLoading) return <Loader />;
  let userDiscounts = [];
  if (userDiscountResult.isSuccess) {
    userDiscounts = userDiscountResult.data.data.map((d) => d.discount.id);
  }

  return (
    <>
      <MetaData title={"BGSS"} />
      <section className="row mt-3 justify-content-center  gap-3 px-5 px-lg-0">
        {discountResult.isSuccess &&
          discountResult?.data?.data.map((d) => (
            <div className="col-12 col-lg-5 p-0">
              <DiscountCard
                discount={d ?? {}}
                isDisabled={
                  d.expire ||
                  d.defaultQuantity === 0 ||
                  userDiscounts.includes(d.id)
                }
                applyMessage={userDiscounts.includes(d.id) ? "Saved" : "Save"}
                onApply={(e) => {
                  if (!isAuthenticated) {
                    return navigate("/login");
                  }
                  const isCustomer = roles?.includes("ROLE_CUSTOMER");
                  if (!isCustomer) {
                    toast.error("Only customers are allowed to save discoount");
                    return;
                  }
                  applyDiscount(d.id);
                }}
              />
            </div>
          ))}
        {discountResult.isError && <h5> Couldn't get data</h5>}
        {discountResult.isLoading && (
          <h5>
            <Loader></Loader>
          </h5>
        )}
      </section>
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
      ></div>

      <div className="row">
        <div className="col-6 col-md-3">
          <Filters />
        </div>
        <div className="col-6 col-md-9">
          <h1
            id="products_heading"
            className="text-secondary border-border-bottom "
          >
            Product List
          </h1>
          <div className="col-12 col-md-6 mt-3 ms-auto">
            <Search />
          </div>
          <section id="products" className="mt-5">
            <div className="row">
              {products?.length ? (
                products?.map((product) => (
                  <ProductItem data={product} columnSize={columnSize} />
                ))
              ) : (
                <div className="display-6 text-center my-5 m-auto">
                  {searchParams.size > 0
                    ? "No product found"
                    : "No Product Yet"}
                </div>
              )}
            </div>
          </section>
          <CustomPagination
            resPerPage={data?.resPerPage}
            filteredProductsCount={data?.filteredProductsCount}
          />
        </div>
      </div>
    </>
  );
};

export default ProductListPage;
