import React, { useState, useEffect } from "react";
import {
  Button,
  Form,
  FormControl,
  FormLabel,
  InputGroup,
  Modal,
} from "react-bootstrap";
import InputGroupText from "react-bootstrap/esm/InputGroupText";
import toast from "react-hot-toast";
import { FaCoins, FaDollarSign } from "react-icons/fa";
import { useSelector } from "react-redux";
import { Link, useNavigate } from "react-router-dom";
import * as Yup from "yup";
import {
  GOLD_UNIT_CONVERT,
  conversionFactors,
  weightConverter,
} from "../../helpers/converters";
import { currencyFormat } from "../../helpers/helpers";
import { useGetPaymentUrlMutation } from "../../redux/api/paymentApi";
import { useRequestWithdrawGoldMutation } from "../../redux/api/transactionApi";
import OTPWithdrawPage from "../otp/OTPCheckFormWIthdraw"; // Import Modal OTP component
import { LoadingButton } from '@mui/lab';
import { Autocomplete } from "@mui/lab";
import { TextField } from "@mui/material";



function UserStorage({ separate, options }) {
  const { user } = useSelector((state) => state.auth);
  const [showDeposit, setShowDeposit] = useState(false);
  const [showWithdraw, setShowWithdraw] = useState(false);
  const [showOTP, setShowOTP] = useState(false);
  const [withdrawId, setWithdrawId] = useState(null); // State to store withdraw ID
  const [withdrawSuccess, setWithdrawSuccess] = useState(false);
  const [getPaymentUrl, { error: paymentError }] = useGetPaymentUrlMutation();
  const [paymentRequest, setPaymentRequest] = useState({
    price: 0,
    userInfoId: user?.userId,
    username: user?.username,
  });
  const [requestWithdrawGold, { error: withdrawError }] =
    useRequestWithdrawGoldMutation();
  const [weightToWithdraw, setWeightToWithdraw] = useState(0);
  const [unit, setUnit] = useState("Mace");
  const tempPrice = paymentRequest.price / 24000;
  const navigate = useNavigate();
  const handleClose = () => {
    setShowDeposit(false);
    setShowWithdraw(false);
    setShowOTP(false); // Close OTP modal if open
  };
  const [value, selectValue] = useState("tOz");
  const [convertedWeight, setConvertedWeight] = useState(0);

  const [isLoading, setIsLoading] = useState(false);


//   const unitOptions = Object.keys(GOLD_UNIT_CONVERT).map(key => ({
//   label: GOLD_UNIT_CONVERT[key],
//   value: key
// }));


const unitOptions = Object.keys(conversionFactors).map((key) => ({
  label: conversionFactors[key].label || key, // Sử dụng label nếu có, nếu không thì dùng key
    value: key
}));


const renderOption = (option) => {
  const convertedWeight = weightConverter(
    inventory?.totalWeightOz,
    "tOz",
    option.value
  );
  return `${option.label}`;
};


const [selectedUnit, setSelectedUnit] = useState(unitOptions[0]);

useEffect(() => {
  console.log("selectedUnit.value:", selectedUnit.value);
  const newWeight = weightConverter(
    weightToWithdraw,
    selectedUnit.value,
    "tOz"
  );
  setConvertedWeight(newWeight);
}, [selectedUnit, weightToWithdraw]);



  if (!user) return null;
  const {
    userInfo: { inventory, balance },
  } = user;

  // Validation schema for deposit amount
  const depositSchema = Yup.object().shape({
    price: Yup.number()
      .typeError("Price must be a number")
      .min(10000, "Minimum quantity is 10,000")
      .positive("The price must be positive")
      .required("Please enter price"),
  });

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      setIsLoading(true);
      await depositSchema.validate(
        { price: paymentRequest.price },
        { abortEarly: false }
      );
      const result = await getPaymentUrl(paymentRequest).unwrap();
      document.location.href = result;
      setIsLoading(false);
    } catch (error) {
      if (error instanceof Yup.ValidationError) {
        error.errors.forEach((errorMessage) => toast.error(errorMessage));
      } else {
        toast.error("Error: Can't get payment url");
      }
    }
  };

  // Function to handle withdrawal submission
  const handleSubmitWithdraw = async (e) => {
    e.preventDefault();
    try {
      setIsLoading(true);
      await withdrawSchema.validate(
        { weightToWithdraw },
        { abortEarly: false }
      );
      console.log(
        "Withdrawal request data:",
        weightToWithdraw,
        GOLD_UNIT_CONVERT[unit.toUpperCase()]
      );
      const response = await requestWithdrawGold({
        weightToWithdraw,
        unit: GOLD_UNIT_CONVERT[unit.toUpperCase()],
      }).unwrap();
      setWithdrawId(response.data.id); // Store the withdraw ID

      // Set the withdraw success state to true
      setWithdrawSuccess(true);
      // Show OTP modal after successful withdrawal request
      setShowOTP(true);
      setIsLoading(false);
    } catch (error) {
      if (error instanceof Yup.ValidationError) {
        error.errors.forEach((errorMessage) => toast.error(errorMessage));
        setIsLoading(false);
      } else {
        setIsLoading(false);
        toast.error(error.data.message);
      }
    }
  };

  const handleOTPSubmit = async (otp) => {
    try {
      // Handle OTP submission logic here
      // If OTP is verified successfully, you can close the OTP modal
      // and show a success message
      console.log("OTP submitted:", otp);
      // For demo purpose, close the OTP modal and show success message
      setShowOTP(false);
      toast.success("OTP submitted successfully!");
    } catch (error) {
      console.error("Error submitting OTP:", error);
      toast.error("Error submitting OTP");
    }
  };
  const keys = Object.keys(conversionFactors);
  // keys?.sort((a, b) => a === value && -1);
  const minMace = 0;
  const maxMace = Math.floor(
    weightConverter(inventory?.totalWeightOz, "tOz", "Mace")
  );
  let min = Math.ceil(weightConverter(minMace, "Mace", unit));
  const max = Math.floor(weightConverter(maxMace, "Mace", unit));
  if (min < 1 && max != 0) min = 1;
  // Validation schema for withdraw amount
  const withdrawSchema = Yup.object().shape({
    weightToWithdraw: Yup.number()
      .integer("withdraw amount must be a non decimal number")
      .max(maxMace, `Not enough gold to withdraw`)
      // .moreThan(0, `Invalid withdraw amount`)
      .positive("The quantity must be positive")
      .required("Please enter quantity"),
  });
  return (
    <>
      <p className="text d-flex flex-column ">
        <div className="position-relative">
          Balance: <b className=" gold ">{currencyFormat(balance?.amount)}</b>
        </div>

        <div className="position-relative">
          Inventory: <b>{inventory?.totalWeightOz + " tOz"}</b>
        </div>
      </p>
      <div className="btn-group d-flex justify-content-center">
        <Link
          className="btn-secondary btn btn-sm ms-auto"
          style={{ fontSize: "0.7em" }}
          onClick={() => setShowDeposit(true)}
        >
          <FaDollarSign /> Deposit
        </Link>
        <Button
          className="btn-secondary btn btn-sm"
          style={{
            fontSize: "0.7em",
            // backgroundColor: "#ffa500 !important",
            backgroundColor: "#ffa500 !important", // Màu cam nhạt
            color: "#fff",
            margin: "0 auto", // Căn giữa theo chiều ngang
          }}
          onClick={() => setShowWithdraw(true)}
        >
          <FaCoins /> Withdraw Gold
        </Button>
      </div>
      <Modal show={showDeposit} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Deposit</Modal.Title>
        </Modal.Header>
        <div className="modal-body">
          <div className="position-relative mb-3">
            Balance: <b className=" gold ">{currencyFormat(balance?.amount)}</b>
          </div>
          <Form id="deposit" onSubmit={handleSubmit}>
            <Form.Group>
              <FormLabel>Amount to Deposit</FormLabel>
              <InputGroup>
                <FormControl
                  name="price"
                  type="number"
                  min={10000}
                  value={paymentRequest?.price}
                  onChange={(e) =>
                    setPaymentRequest({
                      ...paymentRequest,
                      price: e.target.value,
                    })
                  }
                />
                <InputGroupText>VND</InputGroupText>
              </InputGroup>
              <Form.Label>Temp Price: {currencyFormat(tempPrice)} </Form.Label>
            </Form.Group>
          </Form>
        </div>
        <Modal.Footer>
          <LoadingButton variant="outlined" form="deposit" type='submit' loading={isLoading}
          sx={{
            backgroundColor: 'orange',
            color: 'white',
            '&:hover': {
              backgroundColor: 'darkorange',
            },
          }}
          >
            Deposit
          </LoadingButton>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
      {/* Withdraw */}
      <Modal show={showWithdraw} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Withdraw</Modal.Title>
        </Modal.Header>
        {/* <div className="modal-body">
          <div className="position-relative mb-3">
            Inventory:{" "}
            <b className="me-1">
              {weightConverter(inventory?.totalWeightOz, "tOz", value)}
            </b>

            {keys.map((e) => (
              <span
                className={`me-2 btn-link text-decoration-none  ${
                  e === value && "fw-bold "
                }`}
                style={{
                  cursor: "pointer",
                }}
                onClick={(ev) => selectValue(e)}
              >
                {e}
              </span>
              
            ))}
          
          </div> */}
          <div className="modal-body">
  <div className="position-relative mb-3">
    Inventory:{" "}
    <b className="me-1">
    {convertedWeight} TROY OUNCES
    </b>
    <Autocomplete
      options={unitOptions}
      getOptionLabel={renderOption}
      renderInput={(params) => <TextField {...params} label="Select Unit" variant="outlined" />}
      value={selectedUnit}
      onChange={(event, newValue) => setSelectedUnit(newValue)}
      sx={{ width: 200, marginTop: 2 }}
      size="small"
    />
  </div>


          <Form id="withdraw">
            <Form.Group>
              <FormLabel>Amount to Withdraw</FormLabel>
              <InputGroup className="m-0">
                <FormControl
                  name="amount"
                  type="number"
                  onKeyDownCapture={(e) => {
                    e.key === "e" && e.preventDefault();
                    e.key === "." && e.preventDefault();
                    e.key === "," && e.preventDefault();
                  }}
                  onChange={(e) => setWeightToWithdraw(e.target.value)}
                />
                <select
                  className="input-group-text"
                  value={unit}
                  onChange={(e) => setUnit(e.target.value)}
                >
                       
                  <option value={"Mace"}>Mace</option>
                  <option value={"Tael"}>Tael</option>
                </select>
              </InputGroup>
              {/* <div className="small text-danger">{withdrawError}</div> */}
              {/* <div className="small text-danger">
                {withdrawError && withdrawError.data.message}
              </div> */}

              <div className="small text-end me-5">{`${min} ${unit} - ${max} ${unit}`}</div>
            </Form.Group>
          </Form>
        </div>
        <Modal.Footer>
          <LoadingButton type="submit" variant="contained" onClick={handleSubmitWithdraw} loading={isLoading}
          sx={{
            backgroundColor: 'orange',
            color: 'white',
            '&:hover': {
              backgroundColor: 'darkorange',
            },
          }}
          >
            Withdraw
          </LoadingButton>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
      {/* Modal OTP */}
      {showOTP && withdrawSuccess && (
        <OTPWithdrawPage
          withdrawId={withdrawId}
          onClose={handleClose}
          show={showOTP}
          onSubmit={handleOTPSubmit}
        />
      )}
    </>
  );
}

export default UserStorage;

// import { useState } from "react";
// import {
//   Button,
//   Form,
//   FormControl,
//   FormLabel,
//   InputGroup,
//   Modal,
// } from "react-bootstrap";
// import InputGroupText from "react-bootstrap/esm/InputGroupText";
// import toast from "react-hot-toast";
// import { FaCoins, FaDollarSign } from "react-icons/fa";
// import { useSelector } from "react-redux";
// import { Link } from "react-router-dom";
// import { GOLD_UNIT_CONVERT, conversionFactors } from "../../helpers/converters";
// import { currencyFormat } from "../../helpers/helpers";
// import { useGetPaymentUrlMutation } from "../../redux/api/paymentApi";
// import { useRequestWithdrawGoldMutation } from "../../redux/api/transactionApi";
// import * as Yup from "yup";

// function UserStorage({ separate, options }) {
//   const { user, roles } = useSelector((state) => state.auth);
//   const [showDeposit, setShowDeposit] = useState(false);
//   const [showWithdraw, setShowWithdraw] = useState(false);
//   const [getPaymentUrl, { data, error, isSuccess }] =
//     useGetPaymentUrlMutation();
//   const [paymentRequest, setPaymentRequest] = useState({
//     price: 0,
//     userInfoId: user?.userId,
//     username: user?.username,
//   });
//   const [requestWithdrawGold, { isLoading: withdrawLoading }] =
//     useRequestWithdrawGoldMutation();
//   const [weightToWithdraw, setWeightToWithdraw] = useState(0);
//   const [unit, setUnit] = useState("Mace");
//   const tempPrice = paymentRequest?.price / 24000;

//   const handleClose = () => {
//     setShowDeposit(false);
//     setShowWithdraw(false);
//   };

//   if (!user) return null;
//   const {
//     userInfo: { inventory, balance },
//   } = user;

//   // Validation schema for deposit amount
//   const depositSchema = Yup.object().shape({
//     price: Yup.number()
//       .typeError("Price must be a number")
//       .min(10000, "Minimum quantity is 10,000")
//       .positive("The price must be positive")
//       .required("Please enter price"),
//   });

//   // Validation schema for withdraw amount
//   const withdrawSchema = Yup.object().shape({
//     weightToWithdraw: Yup.number()
//       .min(10000, "Minimum quantity is 10,000")
//       .positive("The quantity must be positive")
//       .required("Please enter quantity"),
//   });

//   if (options) {
//     const handleSubmit = async (e) => {
//       e.preventDefault();
//       try {
//         await depositSchema.validate(
//           { price: paymentRequest.price },
//           { abortEarly: false }
//         );
//         const result = await getPaymentUrl(paymentRequest).unwrap();
//         document.location.href = result;
//       } catch (error) {
//         if (error instanceof Yup.ValidationError) {
//           error.errors.forEach((errorMessage) => toast.error(errorMessage));
//         } else {
//           toast.error("Error: Can't get payment url");
//         }
//       }
//     };

//     const handleSubmitWithdraw = async (e) => {
//       e.preventDefault();
//       try {
//         await withdrawSchema.validate(
//           { weightToWithdraw },
//           { abortEarly: false }
//         );
//         console.log(
//           "Withdrawal request data:",
//           weightToWithdraw,
//           GOLD_UNIT_CONVERT[unit.toUpperCase()]
//         );
//         await requestWithdrawGold({
//           weightToWithdraw,
//           unit: GOLD_UNIT_CONVERT[unit.toUpperCase()],
//         });
//         toast.success("Withdrawal request successful!");
//         handleClose();
//       } catch (error) {
//         if (error instanceof Yup.ValidationError) {
//           error.errors.forEach((errorMessage) => toast.error(errorMessage));
//         } else {
//           toast.error("Error: Can't withdraw gold");
//         }
//       }
//     };

//     return (
//       <>
//         <p className="text d-flex flex-column ">
//           <div className="position-relative">
//             Balance: <b className=" gold ">{currencyFormat(balance?.amount)}</b>
//           </div>

//           <div className="position-relative">
//             Inventory: <b>{inventory?.totalWeightOz + " tOz"}</b>
//           </div>
//         </p>
//         <div className="btn-group d-flex justify-content-center">
//           <Link
//             className="btn-primary btn btn-sm ms-auto"
//             style={{ fontSize: "0.7em" }}
//             onClick={(e) => setShowDeposit(true)}
//           >
//             <FaDollarSign /> Deposit
//           </Link>
//           <Link
//             className="btn-secondary btn btn-sm me-auto"
//             style={{ fontSize: "0.7em" }}
//             onClick={(e) => setShowWithdraw(true)}
//           >
//             <FaCoins /> Withdraw Gold
//           </Link>
//         </div>
//         <Modal show={showDeposit} onHide={handleClose}>
//           <Modal.Header closeButton>
//             <Modal.Title>Deposit</Modal.Title>
//           </Modal.Header>
//           <div className="modal-body">
//             <div className="position-relative mb-3">
//               Balance: <b className=" gold ">{currencyFormat(balance?.amount)}</b>
//             </div>
//             <Form id="deposit" onSubmit={handleSubmit}>
//               <Form.Group>
//                 <FormLabel>Amount to Deposit</FormLabel>
//                 <InputGroup>
//                   <FormControl
//                     name="price"
//                     type="number"
//                     min={10000}
//                     value={paymentRequest?.price}
//                     onChange={(e) =>
//                       setPaymentRequest({
//                         ...paymentRequest,
//                         price: e.target.value,
//                       })
//                     }
//                   />
//                   <InputGroupText>VND</InputGroupText>
//                 </InputGroup>
//                 <Form.Label>Temp Price: {currencyFormat(tempPrice)} </Form.Label>
//               </Form.Group>
//             </Form>
//           </div>
//           <Modal.Footer>
//             <Button variant="primary" type="submit" form="deposit">
//               Deposit
//             </Button>
//             <Button variant="secondary" onClick={handleClose}>
//               Close
//             </Button>
//           </Modal.Footer>
//         </Modal>
//         <Modal show={showWithdraw} onHide={handleClose}>
//           <Modal.Header closeButton>
//             <Modal.Title>Withdraw</Modal.Title>
//           </Modal.Header>
//           <div className="modal-body">
//             <div className="position-relative mb-3">
//               Balance: <b>{inventory?.totalWeightOz + " tOz"}</b>
//             </div>
//             <Form id="withdraw" onSubmit={handleSubmitWithdraw}>
//               <Form.Group>
//                 <FormLabel>Amount to Withdraw</FormLabel>
//                 <InputGroup>
//                   <FormControl
//                     name="amount"
//                     type="number"
//                     value={weightToWithdraw}
//                     onChange={(e) => setWeightToWithdraw(e.target.value)}
//                   />
//                   <select
//                     className="input-group-text"
//                     value={unit}
//                     onChange={(e) => setUnit(e.target.value)}
//                   >
//                     <option value={"Mace"}>Mace</option>
//                     <option value={"Tael"}>Tael</option>
//                   </select>
//                 </InputGroup>
//               </Form.Group>
//             </Form>
//           </div>
//           <Modal.Footer>
//             <Button variant="primary" type="submit" form="withdraw">
//               Withdraw
//             </Button>
//             <Button variant="secondary" onClick={handleClose}>
//               Close
//             </Button>
//           </Modal.Footer>
//         </Modal>
//       </>
//     );
//   }
// }

// export default UserStorage;
