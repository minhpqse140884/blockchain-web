import { formatDistanceToNow } from "date-fns";
import { MDBDataTable } from "mdbreact";
import { useState } from "react";
import { discountTable } from "../../constants/constants";
import {
  useAddToMyDiscountMutation,
  useGetMyDiscountQuery
} from "../../redux/api/discountApi";
import MetaData from "../layout/MetaData";
import UserLayout from "../layout/UserLayout";

function UserDiscount() {
  // const [getDiscountByCode, { data, isLoading, isError, isSuccess, error }] =
  //   useLazyGetDiscountByCodeQuery();
  const { data: discountData, isLoading: discountIsLoading } =
    useGetMyDiscountQuery();

  const [addToMyDiscount, { isSuccess: added }] = useAddToMyDiscountMutation();

  const [search, setSearch] = useState("");
  const handleSearch = (e) => {
    setSearch(e.target.value);
  };
  const submitHandler = (e) => {
    e.preventDefault();
    // getDiscountByCode(search?.trim());
  };
  // useEffect(() => {
  //   if (isError) {
  //     toast.error(error?.data?.message);
  //   }
  //   if (isSuccess) {
  //   }
  //   if (added) {
  //     toast.success("Discount code added successfully");
  //   }
  // }, [isSuccess, isError, added]);

  const setDiscounts = () => {
    const discounts = discountTable();
    discounts.columns.splice(-2, 0, {
      label: "Valid",
      field: "valid",
      sort: "asc",
    });
    discountData?.data.forEach((data) => {
      // Ensure consistent data handling
      const discount = data.discount;
      discounts.rows.push({
        id: discount.id,
        code: discount.code,
        percentage: (
          <span className="fw-bold orange">{discount.percentage + "%"} </span>
        ),
        dateCreate: discount.dateCreate
          ? formatDistanceToNow(new Date(discount.dateCreate), {
              addSuffix: true,
            })
          : "",
        dateExpire: discount.dateExpire
          ? formatDistanceToNow(new Date(discount.dateExpire), {
              addSuffix: true,
            })
          : "",
        expire: discount.expire ? "Yes" : "No",
        valid: data?.valid ? "No" : "Yes",
        actions: (
          <div className="d-flex">
            <button
              aria-label="btn-delete"
              className="btn btn-outline-danger ms-2"
              onClick={() => {
                // Hiển thị hộp thoại xác nhận
                const confirmDelete = window.confirm(
                  "Are you sure you want to delete this discount?"
                );

                // Nếu người dùng chọn OK (true), thực hiện xóa
                if (confirmDelete) {
                  // deleteDiscountHandler(discount?.id);
                }
              }}
              //   disabled={isDeleteLoading}
            >
              <i className="fa fa-trash"></i>
            </button>
          </div>
        ),
      });
    });
    // discounts !=null && discounts.columns[3].label = "Date Added";
    return discounts;
  };

  return (
    <UserLayout>
      <MetaData title={"My Discount"} />
      <div className="row wrapper ">
        <article className="col-10 col-lg-8 ">
          <h2 className=" text-center">My Discounts</h2>
          {/*<Form className="shadow rounded bg-body" onSubmit={submitHandler}>
            <Form.Group className="d-flex flex-column">
              <InputGroup className="m-0 w-100">
                <Form.Control
                  type="text"
                  placeholder="Search Discount"
                  aria-label="Search Discount"
                  value={search}
                  onChange={handleSearch}
                />
                <Button type="submit" className="m-0">
                  <FaSearch />
                </Button>
              </InputGroup>
              <hr className="mt-3"></hr>
              {isLoading ? (
                <div className="col-1 d-inline-block mx-auto">
                  <Spinner className="orange small"></Spinner>
              </div> */}
              {/*</article>) : data?.data ? (
                <div>
                  <div className="d-flex">
                    <div className="m-2 " readOnly>
                      Code: <b>{data.data?.code}</b>
                    </div>
                    <div className="input-group-text gap-2 ">
                      <FaInfoCircle /> {data.data?.percentage}% Off
                    </div>
                    <span
                      type="button"
                      className="ms-auto p-2 page-link "
                      onClick={(e) => addToMyDiscount(data.data.id)}
                    >
                      <span className="">
                        Apply <FaCheck />
                      </span>
                    </span>
                  </div>
                </div>
              ) : (
                <div className="text-center">
                  No discount found <FaSearch />
                </div>
              )}
            </Form.Group>
          </Form> */}
        </article>
        <div className=" mb-3">
          <MDBDataTable
            data={setDiscounts()}
            className="px-3"
            responsive
            bordered
            striped
            hover
          />
        </div>
      </div>
    </UserLayout>
  );
}
export default UserDiscount;
