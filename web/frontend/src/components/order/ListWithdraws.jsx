import { MDBDataTable } from "mdbreact";
import { useEffect, useState } from "react";
import { Button, Collapse, Form, Modal } from "react-bootstrap";
import toast from "react-hot-toast";
import { FaCaretDown } from "react-icons/fa";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { FormError } from "../../helpers/components/form-error";
import { colorFormatWithdraw } from "../../helpers/format-color";
import { myDateFormat } from "../../helpers/helpers";
import { generateWithdrawQRImg } from "../../helpers/qrcode-helper";
import {
  useCancelWithdrawalMutation,
  useHandleWithdrawalMutation,
  useMyWithdrawListQuery,
} from "../../redux/api/transactionApi";
import Loader from "../layout/Loader";
import OTPPage from "../otp/OTPCheckFormWIthdraw";



const ListWithdraws = () => {
  const { user, roles } = useSelector((state) => state.auth);
  const withdrawList = useMyWithdrawListQuery();
  const { data, isLoading, error } = withdrawList;
  const [otpModalShow, setOtpModalShow] = useState(false);
  const [orderId, setOrderId] = useState(null);
  const [cancelOrder, { isLoading: canceling, error: cancelError, isError }] =
    useCancelWithdrawalMutation();
  const [completeWithDraw, { isLoading: confirming }] =
    useHandleWithdrawalMutation();
  const handleResendOTP = (orderId) => {
    // Hiển thị modal OTP khi nhấn nút Resend OTP
    setOrderId(orderId);
    setOtpModalShow(true);
  };

  const handleCloseOTPModal = () => {
    setOtpModalShow(false);
  };
  const actionStr = "COMPLETE_WITHDRAWAL";
  useEffect(() => {
    if (error) {
      toast.error(error?.data?.message);
    }
  }, [error]);

  const setWithDraw = () => {
    const orders = {
      columns: [
        {
          label: "ID",
          field: "id",
          sort: "asc",
        },

        {
          label: "Transaction Date",
          field: "transactionDate",
          sort: "asc",
        },
        {
          label: "Updated Date",
          field: "updateDate",
          sort: "asc",
        },
        {
          label: "Total Amount",
          field: "totalAmount",
          sort: "asc",
        },

        {
          label: "WithdrawQrCode",
          field: "withdrawQrCode",
          sort: "asc",
        },
        {
          label: "Status",
          field: "status",
          sort: "asc",
        },
        {
          label: "Actions",
          field: "actions",
          sort: "asc",
        },
      ],
      rows: [],
    };
    data?.data?.forEach((withdraw) => {
      console.log(withdraw);
      orders.rows.push({
        id: withdraw?.id,
        status: (
          <div
            className={`${colorFormatWithdraw(
              withdraw?.status?.toUpperCase()
            )} fw-bold`}
          >
            {withdraw?.status?.toUpperCase()}
          </div>
        ),
        transactionDate: myDateFormat(withdraw?.transactionDate),
        updateDate: myDateFormat(withdraw?.updateDate),
        // totalAmount: GOLD_UNIT_CONVERT_2[withdraw?.amount] + "Mace",
        totalAmount: withdraw?.amount + " " + withdraw?.goldUnit,
        withdrawQrCode: withdraw?.withdrawQrCode,
        userInfoId: withdraw?.userInfoId || "none",
        actions: (
          <div className="d-flex">
            <button
              aria-label="View withdraw"
              className="btn btn-light border "
              onClick={(e) => showModal(withdraw)}
            >
              <i className="fa fa-eye"></i>
            </button>
            {withdraw?.status === "COMPLETED" && (
              <Link
                to={`/invoice/withdraw/${withdraw?.id}`}
                className="btn btn-success ms-2"
              >
                <i className="fa fa-print"></i>
              </Link>
            )}
            {/* Thêm nút resend OTP */}
            {withdraw?.status === "UNVERIFIED" && (
              <>
                <button
                  className="btn btn-info ms-2"
                  onClick={() => handleResendOTP(withdraw.id)}
                >
                  Resend OTP
                </button>
              </>
            )}
          </div>
        ),
      });
    });

    return orders;
  };
  //Modal Box
  const [show, setShow] = useState(false);
  const [modal, setModal] = useState({});
  const handleClose = () => {
    setShow(false);
  };
  const showModal = (value) => {
    setShow(!!value);
    setModal(value);
  };

  const [reason, setReason] = useState("");
  const [errors, setErrors] = useState({});
  const [open, setOpen] = useState(false);
  const onChange = (e) => {
    setReason(e.target.value);
  };

  const onSubmit = async (e) => {
    e.preventDefault();
    if (reason.length === 0)
      setErrors({
        ...errors,
        reason: "Please add a reason",
      });
    try {
      const request = {
        withdrawalId: modal.id,
        reason: reason,
      };
      await cancelOrder(request).unwrap();
      withdrawList.refetch();
      setShow(false);
      // Handle successful cancellation response (e.g., show success message)
    } catch (error) {
      console.log(error);
      toast.error(error?.data?.message);
      // Handle error response (e.g., show error message)
    }
  };
  const handleConfirm = async () => {
    await completeWithDraw({
      id: modal?.id,
      actionStr,
    })
      .unwrap()
      .catch((error) => {
        console.error(error);
        toast.error(error?.data?.message);
      });
    withdrawList.refetch();
    setShow(false);
  };
  if (isLoading) return <Loader />;

  return (
    <>
      <h1 className="my-2 px-5">{data?.length} Withdraws</h1>
      <MDBDataTable
        data={setWithDraw()}
        className="px-5 content mt-5"
        bordered
        striped
        hover
        responsive
      />
      <style jsx>{`
        .dataTables_wrapper .dataTables_length,
        .dataTables_wrapper .dataTables_filter,
        .dataTables_wrapper .dataTables_info,
        .dataTables_wrapper .dataTables_paginate {
          margin-bottom: 20px; /* Điều chỉnh giá trị theo ý muốn của bạn */
        }
      `}</style>
      <Modal show={show} onHide={handleClose} size={"lg"}>
        <Modal.Header closeButton>
          <Modal.Title>Withdraw Detail</Modal.Title>
        </Modal.Header>
        <Modal.Body className="position-relative">
          {/* <p>
            <b>ID:</b> {modal?.id}
          </p> */}
          <p>
            <b>Amount:</b> {modal?.amount} {modal?.goldUnit}
          </p>
          <p>
            <b>Transaction Date:</b> {myDateFormat(modal?.transactionDate)}
          </p>
          <p>
            <b>Updated Date:</b> {myDateFormat(modal?.updateDate)}
          </p>
          <p>
            <b>Customer's:</b> {user?.userInfo?.firstName} {user?.userInfo?.lastName}
          </p>
          <div className="position-absolute bg-white rounded-2 end-0 top-0 mx-5 my-3 p-3 shadow user-select-none ">
            <img
              src={generateWithdrawQRImg(modal?.withdrawQrCode)}
              alt={`${modal?.withdrawQrCode}`}
            />
            <div className="text-center user-select-all mt-2">
              {modal?.withdrawQrCode}
            </div>
          </div>
          {modal?.cancellationMessages?.length > 0 ? (
            <div>
              <hr></hr>
              <b>Cancellation Messages:</b>
              {/* {JSON.stringify(modal.cancellationMessages)} */}
              {modal.cancellationMessages.map((message, index) => (
                <p className="ps-3">
                  <div>
                    <b>Reason:</b> {message.reason}
                  </div>
                  <div>
                    <b>Sender:</b>{" "}
                    {message.sender === user?.username ? (
                      <b>You</b>
                    ) : (
                      message.sender
                    )}
                  </div>
                  <div>
                    <b>Receiver:</b>{" "}
                    {message.receiver === user?.username ? (
                      <b>You</b>
                    ) : (
                      message.receiver
                    )}
                  </div>
                </p>
              ))}
            </div>
          ) : (
            <>
              {modal?.status === "PENDING" && (
                <>
                  <Button
                    onClick={() => setOpen(!open)}
                    variant="danger"
                    aria-controls="example-collapse-text"
                    aria-expanded={open}
                    className="mb-2"
                  >
                    Cancel Withdraw <FaCaretDown />
                  </Button>
                  <Collapse
                    in={open}
                    value={modal?.id}
                    title="Cancel Withdraws"
                  >
                    <Form onSubmit={onSubmit}>
                      <textarea
                        className="col-12 form-control"
                        rows="8"
                        name="reason"
                        placeholder="reason"
                        onChange={onChange}
                        style={{ minHeight: "200px", resize: "vertical" }}
                      />
                      <FormError errorData={errors} name={"reason"} />
                      <button className="btn btn-primary mt-2">Submit</button>
                    </Form>
                  </Collapse>
                </>
              )}
            </>
          )}
        </Modal.Body>
      
      </Modal>
      {/* Modal OTP */}
      {otpModalShow && (
        <OTPPage
          withdrawId={orderId}
          onClose={handleCloseOTPModal} // Cập nhật trạng thái khi đóng modal
        />
      )}
    </>
  );
};
export default ListWithdraws;
