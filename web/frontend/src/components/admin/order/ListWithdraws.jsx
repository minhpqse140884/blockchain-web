import { Popconfirm } from "antd";
import { MDBDataTable } from "mdbreact";
import { useEffect, useState } from "react";
import { Button, Collapse, Form, Modal } from "react-bootstrap";
import toast from "react-hot-toast";
import { FaCaretDown, FaCheck, FaCheckCircle } from "react-icons/fa";
import { useSearchParams } from "react-router-dom";
import { FormError } from "../../../helpers/components/form-error";
import { colorFormatWithdraw } from "../../../helpers/format-color";
import { myDateFormat } from "../../../helpers/helpers";
import { generateWithdrawQRImg } from "../../../helpers/qrcode-helper";
import {
  useCancelWithdrawalMutation,
  useHandleWithdrawalMutation,
  useWithdrawListQuery,
} from "../../../redux/api/transactionApi";
import Loader from "../../layout/Loader";

const ListWithdraws = ({ params }) => {
  const [searchParams] = useSearchParams();
  const withdrawList = useWithdrawListQuery();
  const { data, isLoading, error } = withdrawList;
  const [cancelOrder, { isLoading: canceling }] = useCancelWithdrawalMutation();
  const [confirmWithDraw, { isLoading: confirming }] =
    useHandleWithdrawalMutation();
  const actionStr = "CONFIRM_WITHDRAWAL";
  useEffect(() => {
    if (error) {
      toast.error(error?.data?.message);
    }
  }, [error]);

  const setWithDraw = () => {
    const orders = {
      columns: [
        {
          label: "ID",
          field: "id",
          sort: "asc",
        },

        {
          label: "Transaction Date",
          field: "transactionDate",
          sort: "asc",
        },
        {
          label: "Update Date",
          field: "updateDate",
          sort: "asc",
        },
        {
          label: "Total Amount",
          field: "totalAmount",
          sort: "asc",
        },
        {
          label: "WithdrawQrCode",
          field: "withdrawQrCode",
          sort: "asc",
        },
        {
          label: "Status",
          field: "status",
          sort: "asc",
        },
        {
          label: "Actions",
          field: "actions",
          sort: "asc",
        },
      ],
      rows: [],
    };
    data?.data?.forEach((withdraw) => {
      orders.rows.push({
        id: withdraw?.id,
        status: (
          <div
            className={`${colorFormatWithdraw(
              withdraw?.status?.toUpperCase()
            )} fw-bold`}
          >
            {withdraw?.status?.toUpperCase()}
          </div>
        ),
        transactionDate: myDateFormat(withdraw?.transactionDate),
        updateDate: myDateFormat(withdraw?.updateDate),
        totalAmount: withdraw?.amount + " TOZ",
        withdrawQrCode: withdraw?.withdrawQrCode,
        userInfoId: withdraw?.userInfoId || "none",
        actions: (
          <>
            <button
              aria-label="View withdraw"
              className="btn btn-light  "
              onClick={(e) => showModal(withdraw)}
            >
              <i className="fa fa-eye"></i>
            </button>
          </>
        ),
      });
    });

    return orders;
  };
  //Modal Box
  const [show, setShow] = useState(false);
  const [modal, setModal] = useState({});
  const handleClose = () => {
    setShow(false);
  };
  const showModal = (value) => {
    setShow(!!value);
    setModal(value);
  };

  const [reason, setReason] = useState("");
  const [errors, setErrors] = useState({});
  const [open, setOpen] = useState(false);
  const onChange = (e) => {
    setReason(e.target.value);
  };

  const onSubmit = async (e) => {
    e.preventDefault();
    if (reason.length === 0)
      setErrors({
        ...errors,
        reason: "Please add a reason",
      });
    try {
      const request = {
        withdrawalId: modal.id,
        reason: reason,
      };
      await cancelOrder(request).unwrap();
      withdrawList.refetch(params);
      // Handle successful cancellation response (e.g., show success message)
      setShow(false);
    } catch (error) {
      console.log(error);
      toast.error(error?.data?.message);
      // Handle error response (e.g., show error message)
    }
  };
  const handleConfirm = async () => {
    await confirmWithDraw({
      id: modal?.id,
      actionStr,
    })
      .unwrap()
      .catch((error) => {
        console.error(error);
        toast.error(error?.data?.message);
      });
    withdrawList.refetch();
    setShow(false);
  };
  if (isLoading) return <Loader />;
  return (
    <>
      <h1 className="my-2 px-5">{data?.data?.length} Withdraws</h1>
      
      <MDBDataTable
        data={setWithDraw()}
        className="px-5 content mt-5"
        bordered
        striped
        hover
        responsive
      />
      <style jsx>{`
        .dataTables_wrapper .dataTables_length,
        .dataTables_wrapper .dataTables_filter,
        .dataTables_wrapper .dataTables_info,
        .dataTables_wrapper .dataTables_paginate {
          margin-bottom: 20px; /* Điều chỉnh giá trị theo ý muốn của bạn */
        }
      `}</style>
      <Modal show={show} onHide={handleClose} size={"lg"}>
        <Modal.Header closeButton>
          <Modal.Title>Withdraw Detail</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>
            <b>ID:</b> {modal?.id}
          </p>
          <p>
            <b>Amount:</b> {modal?.amount} tOz
          </p>
          <p>
            <b>Transaction Date:</b> {myDateFormat(modal?.transactionDate)}
          </p>
          <p>
            <b>Updated Date:</b> {myDateFormat(modal?.updateDate)}
          </p>
          <p>
            <p>
              <b>QrCode:</b> {modal?.withdrawQrCode}
            </p>
            <b>User ID:</b> {modal?.userInfoId}
          </p>
          <div className="position-absolute bg-white rounded-2 end-0 top-0 mx-5 my-3 p-3 shadow user-select-none ">
            <img
              src={generateWithdrawQRImg(modal?.withdrawQrCode)}
              alt={`${modal?.withdrawQrCode}`}
            />
            <div className="text-center user-select-all mt-2">
              {modal?.withdrawQrCode}
            </div>
          </div>
          {modal?.cancellationMessages?.length > 0 ? (
            <div>
              <hr></hr>
              <b>Cancellation Messages:</b>
              {/* {JSON.stringify(modal.cancellationMessages)} */}
              {modal.cancellationMessages.map((message, index) => (
                <p className="ps-3">
                  <div>
                    <b>Sender:</b> {message.sender}
                  </div>
                  <div>
                    <b>Receiver:</b> {message.receiver}
                  </div>
                  <div>
                    <b>Reason:</b> {message.reason}
                  </div>
                </p>
              ))}
            </div>
          ) : (
            <>
              {modal.status !== "COMPLETED" ? (
                <Button
                  onClick={() => setOpen(!open)}
                  variant="danger"
                  aria-controls="example-collapse-text"
                  aria-expanded={open}
                  className="mb-2"
                >
                  Cancel Withdraw <FaCaretDown />
                </Button>
              ): <div><FaCheck></FaCheck> Withdraw Completed</div> }
              <Collapse in={open} value={modal?.id} title="Cancel Withdraws">
                <Form onSubmit={onSubmit}>
                  <textarea
                    className="col-12 form-control  "
                    rows="8"
                    name="reason"
                    placeholder="reason"
                    onChange={onChange}
                    style={{ minHeight: "200px", resize: "vertical" }}
                  />
                  <FormError errorData={errors} name={"reason"} />
                  <button className="btn btn-primary mt-2">Submit</button>
                </Form>
              </Collapse>
            </>
          )}
        </Modal.Body>
        <Modal.Footer>
          {modal?.status === "PENDING" && (
            <Popconfirm
              placement="bottom"
              icon={<FaCheckCircle className="mx-2 text-success" />}
              title="Confirmation"
              description="Are you sure to confirm this order?"
              onConfirm={handleConfirm}
              okText="Yes"
              cancelText="No"
            >
              <Button variant="success" disabled={confirming}>
                {confirming ? (
                  <>Confirming...</>
                ) : (
                  <>
                    Confirm Withdraw <FaCheckCircle />
                  </>
                )}
              </Button>
            </Popconfirm>
          )}
          {/* {modal?.status === "CONFIRMED" && (
            <Popconfirm
              placement="topRight"
              icon={<FaCheckCircle className="mx-2 text-success" />}
              title="Confirmation"
              description="Are you sure to complete withdraw?"
              onConfirm={handleConfirm}
              okText="Yes"
              cancelText="No"
            >
              <Button variant="success" disabled={confirming}>
                {confirming ? (
                  <>Confirming...</>
                ) : (
                  <>
                    Complete Withdraw <FaCheckCircle />
                  </>
                )}
              </Button>
            </Popconfirm>
          )} */}
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};
export default ListWithdraws;
