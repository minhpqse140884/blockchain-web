import { MDBDataTable } from "mdbreact";
import { useEffect } from "react";
import { Form } from "react-bootstrap";
import toast from "react-hot-toast";
import { Link, useSearchParams } from "react-router-dom";
import { GOLD_UNIT_CONVERT_2 } from "../../../helpers/converters";
import { currencyFormat } from "../../../helpers/helpers";
import { useTransactionListQuery } from "../../../redux/api/transactionApi";
import Loader from "../../layout/Loader";

const ListTransactions = ({}) => {
  const [searchParams] = useSearchParams();
  const { data, isLoading, error } = useTransactionListQuery();
  useEffect(() => {
    if (error) {
      toast.error(error?.data?.message);
    }
  }, [error]);
  const setTransactions = () => {
    const transactions = {
      columns: [
        {
          label: "ID",
          field: "id",
          sort: "asc",
        },
        {
          label: "Price",
          field: "pricePerOunce",
          sort: "asc",
        },
        {
          label: "Quantity",
          field: "quantity",
          sort: "asc",
        },
        {
          label: "Gold Unit",
          field: "goldUnit",
          sort: "asc",
        },

        {
          label: "Total",
          field: "totalCostOrProfit",
          sort: "asc",
        },
        {
          label: "Status",
          field: "status",
          sort: "asc",
        },
        {
          label: "Confirming Party",
          field: "confirmingParty",
          sort: "asc",
        },
        // {
        //   label: "Actions",
        //   field: "actions",
        //   sort: "asc",
        // },
      ],
      rows: [],
    };
    data?.forEach((transaction) => {
      transactions.rows.push({
        id: transaction?.id,
        totalCostOrProfit: (
          <div
            className={`fw-bold
                ${transaction?.transactionType === "SELL" && "text-success"}
                ${transaction?.transactionType === "BUY" && "text-danger"}`}
          >
            {currencyFormat(transaction?.totalCostOrProfit)}
          </div>
        ),
        goldUnit: transaction?.goldUnit,
        status: transaction?.transactionVerification,
        quantity: `${transaction?.quantity} ${
          GOLD_UNIT_CONVERT_2[transaction?.goldUnit]
        }`,
        pricePerOunce: currencyFormat(transaction?.pricePerOunce),
        confirmingParty: transaction?.confirmingParty,
        actions: (
          <>
            <Link
              to={`/me/transaction/${transaction?.id}`}
              className="btn btn-primary"
            >
              <i className="fa fa-eye"></i>
            </Link>
          </>
        ),
      });
    });

    return transactions;
  };
  if (isLoading) return <Loader />;

  return (
    <>
      <p>
        <Form>
          <Form.Group></Form.Group>
        </Form>
      </p>
      <h1 className="my-2 px-5">{data?.length} Transactions</h1>
      <div className="mt-5 content">
        <p className="text-50">
          Total prices are calculated using Troy Ounces (tOz)
        </p>

        <MDBDataTable
          data={setTransactions()}
          className="px-5"
          bordered
          striped
          hover
          responsive
        />
      </div>
      <style jsx>{`
        .dataTables_wrapper .dataTables_length,
        .dataTables_wrapper .dataTables_filter,
        .dataTables_wrapper .dataTables_info,
        .dataTables_wrapper .dataTables_paginate {
          margin-bottom: 20px; /* Điều chỉnh giá trị theo ý muốn của bạn */
        }
      `}</style>
    </>
  );
};
export default ListTransactions;
