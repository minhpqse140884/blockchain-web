import React, { useEffect, useState } from "react";
import { toast } from "react-hot-toast";
import * as Yup from "yup";

import { Link, useNavigate, useParams } from "react-router-dom";
import { FormError } from "../../../helpers/components/form-error";
import {
  addErrors,
  clearErrors,
} from "../../../helpers/form-validation-helpers";
import {
  useGetCategoriesQuery,
  // useCreateProductMutation,
  useGetProductDetailsQuery,
  useUpdateProductMutation,
} from "../../../redux/api/productsApi";
import AdminLayout from "../../layout/AdminLayout";
import MetaData from "../../layout/MetaData";

const UpdateProduct = () => {
  const navigate = useNavigate();
  const params = useParams();

  const [product, setProduct] = useState({
    productName: "",
    description: "",
    price: "",
    unitOfStock: "",
    categoryId: undefined,
  });
  // console.log(product);
  const { productName, description, price, unitOfStock, categoryId } = product;

  const [updateProduct, { isLoading, error, isSuccess }] =
    useUpdateProductMutation();

  const { data } = useGetProductDetailsQuery(params?.id);
  const { data: categoryData } = useGetCategoriesQuery();

  const productSchema = Yup.object().shape({
    productName: Yup.string().required("Product is required"),
    price: Yup.number()
      .required("Price is required")
      .moreThan(0),
    description: Yup.string().required("Description is required").max(2000,"Description must be smaller than 2000 characters"),
    // categoryId: Yup.string().required(
    //   "please choose a Category "
    // ),
    unitOfStock: Yup.number()
      .required("Stock is required")
      .min(0, "Please choose Valid Stock Ammount"),
  });
  useEffect(() => {
    // console.log(data?.data);
    if (data?.data) {
      setProduct({
        productName: data?.data?.productName,
        description: data?.data?.description,
        price: data?.data?.price,
        unitOfStock: data?.data?.unitOfStock,
        categoryId: data?.data?.category?.id,
      });
    }

    if (error) {
      toast.error(error?.data?.message);
    }

    if (isSuccess) {
      toast.success("Product updated");
      navigate("/admin/products");
    }
  }, [error, isSuccess, data]);

  const [errors, setErrors] = useState({});

  const onChange = (e) => {
    setProduct({ ...product, [e.target.name]: e.target.value });
    // Optionally reset the error state when the user starts typing
    if (errors[e.target.name]) {
      setErrors({ ...errors, [e.target.name]: undefined });
      clearErrors(document, errors, e.target.name);
    }
  };

  const submitHandler = (e) => {
    e.preventDefault();
    productSchema
      .validate(product, { abortEarly: false })
      .then(() => {
        updateProduct({ id: params?.id, body: product });
      })
      .catch((yupError) => {
        const newErrors = {};
        if (yupError.inner) {
          yupError.inner.forEach((error) => {
            newErrors[error.path] = error.message;
          });
        }
        setErrors(newErrors);
        // console.log(newErrors);
        // Show error toast if needed
        toast.error("Please fix the validation errors.");
      });
    // console.log("Updating product with data:", product); // Log the data being sent to the server
  };
  useEffect(() => {
    addErrors(document, errors);
  }, [errors]);
  return (
    <AdminLayout>
      <MetaData title={"Update Product"} />
      <div className="row wrapper">
        <div className="col-10 col-lg-10 mt-5 mt-lg-0">
          <form className="shadow rounded bg-body  " onSubmit={submitHandler}>
            <h2 className="mb-4 position-relative">
              Update Product
              <Link
                to={`/admin/products/${params?.id}/upload_images`}
                style={{
                  backgroundColor: "transparent",
                  color: "var(--bs-btn-active-bg)",
                  borderColor: "var(--bs-btn-active-bg)",
                }}
                className="btn btn-outline-success shadow  end-0 translate-middle-y  position-absolute "
              >
                <i className="fa fa-image"></i>
              </Link>
            </h2>
            {!!data?.data?.imgUrl && (
              <div className="row mt-4">
                <div className="col-md-6 mt-2 m-auto">
                  <div className="card">
                    <img
                      src={data?.data?.imgUrl}
                      alt="Card"
                      className="card-img-top p-2"
                      style={{
                        width: "100%",
                        aspectRatio: "9/6",
                        objectFit: "contain",
                      }}
                    />
                  </div>
                </div>
              </div>
            )}
            <div className="mb-3">
              <label htmlFor="name_field" className="form-label">
                {" "}
                Name{" "}
              </label>
              <input
                type="text"
                id="name_field"
                className="form-control"
                name="productName"
                value={productName}
                onChange={onChange}
              />
              <FormError name="productName" errorData={errors} />
            </div>

            <div className="mb-3">
              <label htmlFor="description_field" className="form-label">
                Description
              </label>
              <textarea
                className="form-control"
                id="description_field"
                rows="8"
                name="description"
                value={description}
                onChange={onChange}
              ></textarea>
              <FormError name="description" errorData={errors} />
            </div>

            <div className="row">
              <div className="mb-3 col">
                <label htmlFor="price_field" className="form-label">
                  {" "}
                  Price{" "}
                </label>
                <input
                  type="text"
                  id="price_field"
                  className="form-control"
                  name="price"
                  value={price}
                  onChange={onChange}
                />
                <FormError name="price" errorData={errors} />
              </div>

              <div className="mb-3 col">
                <label htmlFor="stock_field" className="form-label">
                  {" "}
                  Stock{" "}
                </label>
                <input
                  type="number"
                  id="stock_field"
                  className="form-control"
                  name="unitOfStock"
                  value={unitOfStock}
                  onChange={onChange}
                />
                <FormError name="unitOfStock" errorData={errors} />
              </div>
            </div>
            <div className="row">
              <div className="mb-3 col">
                <label htmlFor="category_field" className="form-label">
                  {" "}
                  Category{" "}
                </label>
                <select
                  className="form-select"
                  id="category_field"
                  name="categoryId"
                  value={categoryId}
                  onChange={onChange}
                >
                  {categoryData?.data?.map((category, index) => (
                    <option key={category.id} value={category.id}>
                      {category.categoryName}
                    </option>
                  ))}
                </select>
                <FormError name="categoryId" errorData={errors} />
              </div>
            </div>
            <button
              type="submit"
              className="btn w-100 py-2"
              disabled={isLoading}
            >
              {isLoading ? "Updating..." : "UPDATE"}
            </button>
          </form>
        </div>
      </div>
    </AdminLayout>
  );
};

export default UpdateProduct;
