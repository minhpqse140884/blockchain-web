import React, { useEffect, useRef, useState } from "react";
import { toast } from "react-hot-toast";
import * as Yup from "yup";

import { useNavigate } from "react-router-dom";
import { FormError } from "../../../helpers/components/form-error";
import {
  addErrors,
  clearErrors,
} from "../../../helpers/form-validation-helpers";
import {
  useCreateProductMutation,
  useGetCategoriesQuery,
} from "../../../redux/api/productsApi";
import AdminLayout from "../../layout/AdminLayout";
import MetaData from "../../layout/MetaData";

const NewProduct = () => {
  const navigate = useNavigate();

  const [newProduct, setNewProduct] = useState({
    productName: "",
    description: "",
    price: 0,
    unitOfStock: 0,
    categoryId: null,
  });

  const { productName, description, price, unitOfStock, categoryId } =
    newProduct;

  const fileInputRef = useRef(null);

  const [createProduct, { isLoading, error, isSuccess }] =
    useCreateProductMutation();
  const { data } = useGetCategoriesQuery();
  useEffect(() => {
    if (data) setNewProduct({ ...newProduct, categoryId: data?.data[0].id });
  }, [data]);
  const productSchema = Yup.object().shape({
    productName: Yup.string().required("Product is required"),
    price: Yup.number()
      .required("Price is required")
      .min(1, "Price must be larger than 0"),
    description: Yup.string()
      .required("Description is required")
      .max(2000, "Description must be smaller than 2000 characters"),
    // categoryId: Yup.string().required(
    //   "please choose a Category (if there is no category that fit the product then create another category)"
    // ),
    unitOfStock: Yup.number()
      .required("Stock is required")
      .min(0, "Please choose Valid Stock Ammount"),
    product_image: Yup.array()
      .of(Yup.mixed().required("Image is required"))
      .required("Image is required"),
  });

  useEffect(() => {
    if (error) {
      toast.error(error?.data?.message);
    }

    if (isSuccess) {
      toast.success("Product created success");
      navigate("/admin/products");
    }
  }, [error, isSuccess]);

  const [errors, setErrors] = useState({});

  const handleResetFileInput = () => {
    if (fileInputRef.current) {
      fileInputRef.current.value = "";
    }
  };
  const [images, setImages] = useState([]);
  const [imagesPreview, setImagesPreview] = useState([]);
  const handleImagePreviewDelete = (image) => {
    const name = "product_image";
    const filteredImagesPreview = imagesPreview.filter((img) => img != image);
    setNewProduct({ ...newProduct, [name]: null });
    setImages(filteredImagesPreview);
    setImagesPreview(filteredImagesPreview);
  };
  const onChange = (e) => {
    setNewProduct({ ...newProduct, [e.target.name]: e.target.value });
    // Optionally reset the error state when the user starts typing
    if (errors[e.target.name]) {
      setErrors({ ...errors, [e.target.name]: undefined });
    }
    clearErrors(document, errors, e.target.name);
  };
  const onChangeImage = (e) => {
    const files = Array.from(e.target.files);
    files.forEach((file) => {
      const reader = new FileReader();

      reader.onload = () => {
        if (reader.readyState === 2) {
          setImagesPreview((oldArray) => [reader.result]);
          setImages((oldArray) => [...files]);
          setNewProduct({ ...newProduct, [e.target.name]: files });
        }
      };
      reader.readAsDataURL(file);
    });
    clearErrors(document, errors, e.target.name);
  };
  useEffect(() => {
    addErrors(document, errors);
  }, [errors]);

  const submitHandler = (e) => {
    e.preventDefault();
    productSchema
      .validate(newProduct, { abortEarly: false })
      .then(() => {
        createProduct(newProduct);
      })
      .catch((yupError) => {
        const newErrors = {};
        if (yupError.inner) {
          yupError.inner.forEach((error) => {
            newErrors[error.path] = error.message;
          });
        }
        setErrors(newErrors);
        // console.log(newErrors);
        // Show error toast if needed
        toast.error("Please fix the validation errors.");
      });
  };

  return (
    <AdminLayout>
      <MetaData title={"Create new Product"} />
      <div className="row wrapper">
        <div className="col-10 col-lg-10 mb-5">
          <form className="shadow rounded bg-body" onSubmit={submitHandler}>
            <h2 className="mb-4">New Product</h2>
            <label className="mb-4">Product Image</label>
            <div className="custom-file mb-3">
              <input
                ref={fileInputRef}
                type="file"
                name="product_image"
                className="form-control"
                id="customFile"
                accept="image/*"
                onChange={onChangeImage}
                onClick={handleResetFileInput}
              />
            </div>
            <FormError name="product_image" errorData={errors} />

            {imagesPreview?.length > 0 && (
              <div className="new-images my-3">
                {/* <p className="gold">New Images:</p> */}
                <div className="row mt-4">
                  {imagesPreview?.map((img) => (
                    <div className="col-md-6 mt-2 m-auto">
                      <div className="card">
                        <img
                          src={img}
                          alt="Card"
                          className="card-img-top p-2"
                          style={{
                            width: "100%",
                            aspectRatio: "9/6",
                            objectFit: "contain",
                          }}
                        />
                        <button
                          aria-label="add Images"
                          style={{
                            backgroundColor: "#dc3545",
                            borderColor: "#dc3545",
                          }}
                          type="button"
                          className="btn btn-block btn-danger cross-button mt-1 py-0"
                          onClick={() => handleImagePreviewDelete(img)}
                        >
                          <i className="fa fa-times"></i>
                        </button>
                      </div>
                    </div>
                  ))}
                </div>
              </div>
            )}
            <div className="mb-3">
              <label htmlFor="name_field" className="form-label">
                {" "}
                Product Name{" "}
              </label>
              <input
                type="text"
                id="name_field"
                className="form-control"
                name="productName"
                value={productName}
                onChange={onChange}
              />
              <FormError name="productName" errorData={errors} />
            </div>
            <div className="mb-3">
              <label htmlFor="description_field" className="form-label">
                Description
              </label>
              <textarea
                className="form-control"
                id="description_field"
                rows="8"
                name="description"
                value={description}
                onChange={onChange}
              ></textarea>
              <FormError name="description" errorData={errors} />
            </div>

            <div className="row">
              <div className="mb-3 col">
                <label htmlFor="price_field" className="form-label">
                  {" "}
                  Price{" "}
                </label>
                <input
                  type="money"
                  id="price_field"
                  className="form-control"
                  name="price"
                  value={price}
                  onChange={onChange}
                />
                <FormError name="price" errorData={errors} />
              </div>

              <div className="mb-3 col">
                <label htmlFor="stock_field" className="form-label">
                  {" "}
                  Stock{" "}
                </label>
                <input
                  type="number"
                  id="stock_field"
                  className="form-control"
                  name="unitOfStock"
                  value={unitOfStock}
                  onChange={onChange}
                />
                <FormError name="unitOfStock" errorData={errors} />
              </div>
            </div>
            <div className="row">
              <div className="mb-3 col">
                <label htmlFor="category_field" className="form-label">
                  {" "}
                  Category{" "}
                </label>
                <select
                  className={`form-select ${
                    errors["categoryId"] && "input-error"
                  } `}
                  id="category_field"
                  name="categoryId"
                  // value={categoryId}
                  onChange={onChange}
                >
                  {data?.data?.map((category, index) => (
                    <option key={category.id} value={category.id}>
                      {category.categoryName}
                    </option>
                  ))}
                </select>
                <FormError name="categoryId" data={errors} />
              </div>
            </div>
            <button
              type="submit"
              className="btn w-100 py-2"
              disabled={isLoading}
            >
              {isLoading ? "Creating..." : "CREATE"}
            </button>
          </form>
        </div>
      </div>
    </AdminLayout>
  );
};

export default NewProduct;
