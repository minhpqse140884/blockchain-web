import { MDBDataTable } from "mdbreact";
import React, { useEffect } from "react";
import { toast } from "react-hot-toast";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import {
  useDeleteCategoryProductMutation,
  useGetCategoryProductsQuery,
} from "../../../redux/api/categoryProductApi";
import AdminLayout from "../../layout/AdminLayout";
import Loader from "../../layout/Loader";
import MetaData from "../../layout/MetaData";

const ListCategoryProducts = () => {
  const { data, isLoading, error } = useGetCategoryProductsQuery();
  const { user, roles } = useSelector((state) => state.auth);
  const [
    deleteCategoryProduct,
    { isLoading: isDeleteLoading, error: deleteError, isSuccess },
  ] = useDeleteCategoryProductMutation();

  useEffect(() => {
    if (error) {
      toast.error(error.message); // Hiển thị thông báo lỗi
    }

    if (deleteError) {
      toast.error(deleteError.message); // Hiển thị thông báo lỗi khi xóa
    }

    if (isSuccess) {
      toast.success("CategoryProduct Deleted"); // Hiển thị thông báo thành công khi sản phẩm được xóa
    }
  }, [error, deleteError, isSuccess]);

  const deleteProductHandler = (id) => {
    deleteCategoryProduct(id);
  };

  const setCategoryProducts = () => {
    const categoriess = {
      columns: [
        {
          label: "ID",
          field: "id",
          sort: "asc",
        },
        {
          label: "Category Name",
          field: "categoryName",
          sort: "asc",
        },
        {
          label: "Actions",
          field: "actions",
          sort: "asc",
        },
      ],
      rows: [],
    };
    data?.data.forEach((categories) => {
      categoriess.rows.push({
        id: categories.id,
        categoryName: categories.categoryName,

        actions: (
          <>
            {roles?.includes("ROLE_ADMIN") && (
              <div
                className="d-flex align-items-center"
                style={{ maxWidth: "45px" }}
              >
                {" "}
                {/* Sử dụng maxWidth để giảm kích thước của khung "Actions" */}
                <div className="me-2">
                  <Link
                    to={`/admin/categoryProducts/${categories?.id}`}
                    className="btn btn-outline-primary"
                  >
                    <i className="fa fa-pencil"></i>
                  </Link>
                </div>
                <div>
                  <button
                    aria-label="delete"
                    className="btn btn-outline-danger"
                    onClick={() => {
                      const confirmDelete = window.confirm(
                        "Are you sure you want to delete this product?"
                      );

                      if (confirmDelete) {
                        deleteProductHandler(categories?.id);
                      }
                    }}
                    disabled={isDeleteLoading}
                  >
                    <i className="fa fa-trash"></i>
                  </button>
                </div>
              </div>
            )}
          </>
        ),
      });
    });

    return categoriess;
  };

  if (isLoading) {
    return (
      <>
        <AdminLayout>
          <MetaData title={"Admin Products"} />
          <Loader />
        </AdminLayout>
      </>
    );
  }

  return (
    <AdminLayout>
      <MetaData title={"All Category Products"} />
      <h1 className="my-2 px-5">{data?.data?.length} Category Products</h1>
      {roles?.includes("ROLE_ADMIN") && (
        <div className="mb-2 px-5 content mt-5">
          <Link
            to="/admin/categoryProducts/new"
            className="btn btn-outline-success me-2"
          >
            <i className="fa fa-plus"></i> Add Category Product
          </Link>
        </div>
      )}
      <MDBDataTable
        data={setCategoryProducts()}
        className="px-5 content"
        bordered
        striped
        hover
      />
      <style jsx>{`
        .dataTables_wrapper .dataTables_length,
        .dataTables_wrapper .dataTables_filter,
        .dataTables_wrapper .dataTables_info,
        .dataTables_wrapper .dataTables_paginate {
          margin-bottom: 20px; /* Điều chỉnh giá trị theo ý muốn của bạn */
        }
      `}</style>
    </AdminLayout>
  );
};

export default ListCategoryProducts;
