import { MDBDataTable } from "mdbreact";
import React, { useEffect } from "react";
import { toast } from "react-hot-toast";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { BASE_PRODUCTIMG } from "../../../constants/constants";
import {
  useDeleteProductMutation,
  useGetAdminProductsQuery
} from "../../../redux/api/productsApi";
import AdminLayout, { AdminLayoutLoader } from "../../layout/AdminLayout";
import MetaData from "../../layout/MetaData";

const ListProducts = () => {
  const { data, isLoading, error } = useGetAdminProductsQuery();
  const { roles } = useSelector((state) => state.auth);
  const [
    deleteProduct,
    { isLoading: isDeleteLoading, error: deleteError, isSuccess },
  ] = useDeleteProductMutation();

  useEffect(() => {
    // console.log(data);
    if (error) {
      toast.error(error?.data?.message);
    }

    if (deleteError) {
      toast.error(deleteError?.data?.message);
    }

    if (isSuccess) {
      toast.success("Product Deleted");
    }
  }, [error, deleteError, isSuccess]);

  const deleteProductHandler = (id) => {
    deleteProduct(id);
  };

  const setProducts = () => {
    const products = {
      columns: [
        {
          label: "ID",
          field: "id",
          sort: "asc",
        },
        {
          label: "Name",
          field: "name",
          sort: "asc",
        },
        {
          label: "Category",
          field: "category",
          sort: "asc",
        },
        {
          label: "Stock",
          field: "stock",
          sort: "asc",
        },
        {
          label: "Images",
          field: "imgUrl",
          sort: "asc",
        },
        {
          label: "Actions",
          field: "actions",
          sort: "asc",
        },
      ],
      rows: [],
    };
    data?.data?.forEach((product) => {
      products.rows.push({
        id: product.id,
        category: product.category.categoryName,
        name: `${
          product?.productName.length > 20
            ? product.productName.substring(0, 20) + "..."
            : product.productName
        }`,
        stock: product?.unitOfStock,
        imgUrl: (
          <img
            className=""
            style={{
              height: "45px",
              maxWidth: "100px",
            }}
            src={product?.imgUrl || BASE_PRODUCTIMG}
            alt={product?.imgUrl}
          ></img>
        ),
        actions: (
          <div className="d-flex align-items-center gap-2 flex-wrap  col-auto">
            <Link
              to={`/product/${product?.id}`}
              className="btn btn-outline-primary"
            >
              <i className="fa fa-eye"></i>
            </Link>
            {roles?.includes("ROLE_ADMIN") && (
              <>
                <Link
                  to={`/admin/products/${product?.id}`}
                  className="btn btn-outline-success"
                >
                  <i className="fa fa-pencil"></i>
                </Link>
                <Link
                  to={`/admin/products/${product?.id}/upload_images`}
                  className="btn btn-outline-success "
                >
                  <i className="fa fa-image"></i>
                </Link>
                <button
                  aria-label="btn-delete"
                  className="btn btn-outline-danger "
                  onClick={() => {
                    // Hiển thị hộp thoại xác nhận
                    const confirmDelete = window.confirm(
                      "Do you sure you want to delete this product?"
                    );

                    // Nếu người dùng chọn OK (true), thực hiện xóa
                    if (confirmDelete) {
                      deleteProductHandler(product?.id);
                    }
                  }}
                  disabled={isDeleteLoading}
                >
                  <i className="fa fa-trash"></i>
                </button>
              </>
            )}
          </div>
        ),
      });
    });

    return products;
  };
  const title = "All Products";
  if (isLoading) {
    return <AdminLayoutLoader title={title} />;
  }

  return (
    <AdminLayout>
      <MetaData title={title} />

      <h1 className="my-2 px-5">{data?.data?.length} Products</h1>
      {roles?.includes("ROLE_ADMIN") && (
        <div className="mb-2 px-5 content mt-5">
          <Link
            to="/admin/products/new"
            className="btn btn-outline-success me-2"
          >
            <i className="fa fa-plus"></i> Add Product
          </Link>
        </div>
      )}
      <MDBDataTable
        data={setProducts()}
        className="px-5 content"
        bordered
        responsive
        striped
        hover
      />
      <style jsx>{`
        .dataTables_wrapper .dataTables_length,
        .dataTables_wrapper .dataTables_filter,
        .dataTables_wrapper .dataTables_info,
        .dataTables_wrapper .dataTables_paginate {
          margin-bottom: 20px; /* Điều chỉnh giá trị theo ý muốn của bạn */
        }
      `}</style>
    </AdminLayout>
  );
};
export default ListProducts;
// {
//   "id": 6,
//   "productName": "Earrings FE60284 ",
//   "description": "The FE60284 earrings are exquisite pieces that radiate timeless beauty. With delicate floral accents and shimmering details, these earrings effortlessly elevate any ensemble. Crafted with precision and elegance, they add a touch of sophistication to every occasion, capturing attention with their graceful charm.\n",
//   "price": 555,
//   "unitOfStock": 49,
//   "category": {
//       "id": 2,
//       "categoryName": "Earrings"
//   },
//   "imgUrl": "Images/Product/PR0006.jpeg",
//   "avgReview": 5
// }
