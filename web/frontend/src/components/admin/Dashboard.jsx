import React, { useEffect, useState } from "react";
import AdminLayout, { AdminLayoutLoader } from "../layout/AdminLayout";

import "react-datepicker/dist/react-datepicker.css";

import {
  DashBoardDateType,
  useCalculateOrderSalesMutation,
  useCalculateTransactionBuySalesMutation,
  useCalculateTransactionSellSalesMutation,
  useCalculateWithdrawMutation,
  useQuantityStatisticsMutation,
} from "../../redux/api/dashBoardApi";
import MetaData from "../layout/MetaData";
import {
  OrdersStatisticsCard,
  PostsStatisticsCard,
  ProductsStatisticsCard,
  StarReviewsDoughnutChart,
  TransactionsStatisticsCard,
  UsersStatisticsCard,
  WithdrawStatisticsCard,
  LineChartCard,
} from "./components/DashboardComponents";
// ChartJS.defaults.global.legend.display = false;
const Dashboard = () => {
  const [getStatistic, { error, isLoading, data: quantityStatistic }] =
    useQuantityStatisticsMutation();
  const [getOrderStatistic, orderResult] = useCalculateOrderSalesMutation();
  const [getTransactionBuyStatistic, transactionBResult] =
    useCalculateTransactionBuySalesMutation();
  const [getTransactionSaleStatistic, transactionSResult] =
    useCalculateTransactionSellSalesMutation();

  const [getWithdrawStatistic, withdrawResult] = useCalculateWithdrawMutation();
  const [options, setOptions] = useState({
    type: DashBoardDateType.DAY,
    monthChose: new Date().getMonth() + 1,
  });

  useEffect(() => {
    getStatistic();
  }, []);
  //leave empty for later

  useEffect(() => {
    getOrderStatistic(options);
    getTransactionBuyStatistic(options);
    getTransactionSaleStatistic(options);
    getWithdrawStatistic(options);
  }, [options]);

  const submitHandler = () => {
    // getDashboardSales({
    //   startDate: new Date(startDate).toISOString(),
    //   endDate: endDate.toISOString(),
    // });
  };
  const title = "Admin Products";
  if (isLoading) {
    return <AdminLayoutLoader title={title} />;
  }
  const quantityProductReviews = quantityStatistic?.data.quantityProductReviews;
  const colors = { backgroundColor: "#d4af375b", borderColor: "#d4af37" };
  const productPieChart = {
    datasets: [
      {
        label: "# of Stars",
        data: quantityProductReviews,
        backgroundColor: [
          colors.backgroundColor,
          colors.backgroundColor,
          colors.backgroundColor,
          colors.backgroundColor,
          colors.backgroundColor,
        ],
        borderColor: [
          colors.borderColor,
          colors.borderColor,
          colors.borderColor,
          colors.borderColor,
          colors.borderColor,
        ],
        borderWidth: 3,
      },
    ],

    // These labels appear in the legend and in the tooltips when hovering different arcs
    labels: ["1", "2", "3", "4", "5"].map((e) => e + " Stars"),
  };
  return (
    <AdminLayout>
      <MetaData title={title} />
      <h2 className="mt-3">Application Statistic</h2>
      <div
        className="d-grid gap-2 justify-content-evenly content "
        style={{
          gridTemplateColumns: "1fr 1fr 1fr 1fr",
        }}
      >
        <ProductsStatisticsCard
          quantityStatistic={quantityStatistic}
          quantityProductReviews={quantityProductReviews}
          isLoading={isLoading}
        />
        <StarReviewsDoughnutChart
          productPieChart={productPieChart}
          isLoading={isLoading}
        />
        <UsersStatisticsCard
          quantityStatistic={quantityStatistic}
          isLoading={isLoading}
        />
        <PostsStatisticsCard
          quantityStatistic={quantityStatistic}
          isLoading={isLoading}
        />
      </div>
      <hr></hr>
      <h2>Sales</h2>
      <div className="p-2 d-flex gap-2 align-items-center mb-3">
        {Object.keys(DashBoardDateType).map((e) => (
          <button
            className={`d-inline-block btn border border-3 rounded-5 p-2 ${
              e === options.type ? "btn-primary" : "hover active"
            }`}
            style={{minWidth:70}}
            onClick={() => {
              setOptions({ ...options, type: e });
            }}
          >
            {e}
          </button>
        ))}
        {options.type === "MONTH" ? (
          <div className="col-auto">
            <select
              className="form-select"
              defaultValue={options.monthChose}
              onChange={(e) =>
                setOptions({ ...options, monthChose: e.target.value })
              }
            >
              {Array.apply(null, { length: 12 }).map((e, index) => {
                return (
                  <option
                    className={`${
                      new Date().getMonth() === index &&
                      "btn-primary text-light"
                    } `}
                    key={index}
                    value={index + 1}
                  >
                    {index + 1}
                  </option>
                );
              })}
            </select>
          </div>
        ) : null}
      </div>
      <div
        className="d-grid gap-2 justify-content-evenly content"
        style={{
          gridTemplateColumns: "1fr 1fr 1fr 1fr",
        }}
      >
        <OrdersStatisticsCard
          orderStatistic={orderResult?.data}
          isLoading={orderResult.isLoading}
        />
        <TransactionsStatisticsCard
          transactionBStatistic={transactionBResult.data}
          transactionSStatistic={transactionSResult.data}
          isLoading={
            transactionSResult.isLoading || transactionBResult.isLoading
          }
        />
        <WithdrawStatisticsCard
          withdrawStatistic={withdrawResult.data}
          isLoading={withdrawResult.isLoading}
        />
        <LineChartCard />
      </div>
      {/* <div className="d-flex justify-content-start align-items-center">
        <div className="mb-3 me-4">
          <label className="form-label d-block">Start Date</label>
        </div>
        <div className="mb-3">
          <label className="form-label d-block">End Date</label>
        </div>
        <button
          className="btn fetch-btn ms-4 mt-3 px-5"
          onClick={submitHandler}
        >
          Fetch
        </button>
      </div> */}
      {/* <div className="row pr-4 my-5">
        <div className="col-xl-6 col-sm-12 mb-3">
          <div className="card text-white bg-success o-hidden h-100">
            <div className="card-body">
              <div className="text-center card-font-size">
                Sales
                <br />
                <b>${data?.totalSales?.toFixed(2)}</b>
              </div>
            </div>
          </div>
        </div>

        <div className="col-xl-6 col-sm-12 mb-3">
          <div className="card text-white bg-danger o-hidden h-100">
            <div className="card-body">
              <div className="text-center card-font-size">
                Orders
                <br />
                <b>{data?.totalNumOrders}</b>
              </div>
            </div>
          </div>
        </div>
      </div>

      <SalesChart salesData={data?.sales} /> */}
      <div className="mb-5"></div>
    </AdminLayout>
  );
};

export default Dashboard;
