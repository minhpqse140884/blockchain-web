import { MDBDataTable } from "mdbreact";
import React, { useEffect } from "react";
import { toast } from "react-hot-toast";
import { Link } from "react-router-dom";
import {
  useDeleteCategoryPostMutation,
  useGetAllCategoryPostQuery,
} from "../../../redux/api/postCategoryApi";
import AdminLayout from "../../layout/AdminLayout";
import Loader from "../../layout/Loader";
import MetaData from "../../layout/MetaData";

const ListCategoryPosts = () => {
  const { data, isLoading, error } = useGetAllCategoryPostQuery();

  const [
    deletePost,
    { isLoading: isDeleteLoading, error: deleteError, isSuccess },
  ] = useDeleteCategoryPostMutation();

  useEffect(() => {
    if (error) {
      toast.error(error?.data?.message);
    }

    if (deleteError) {
      toast.error(deleteError?.data?.message);
    }

    if (isSuccess) {
      toast.success("Post Deleted");
    }
  }, [error, deleteError, isSuccess]);

  const deletePostHandler = (id) => {
    deletePost(id);
  };

  const setPosts = () => {
    const posts = {
      columns: [
        {
          label: "ID",
          field: "id",
          sort: "asc",
        },
        {
          label: "Category Name",
          field: "categoryName",
          sort: "asc",
        },
        {
          label: "Forum ID",
          field: "forumId",
          sort: "asc",
        },
        {
          label: "Forum Hide",
          field: "forumHide",
          sort: "asc",
        },
        {
          label: "Actions",
          field: "actions",
          sort: "asc",
        },
      ],
      rows: [],
    };
    data?.data?.forEach((post) => {
      posts.rows.push({
        id: post.id,
        categoryName: post?.categoryName,
        forumId: post?.forums?.id,
        forumHide: post?.forums?.hide ? "True" : "False", // Display as string "True" or "False"
        actions: (
          <div className="d-flex">
            <Link
              to={`/admin/categoryPosts/${post?.id}`}
              className="btn btn-outline-primary"
            >
              <i className="fa fa-pencil"></i>
            </Link>
            <button
              aria-label="btn-delete"
              className="btn btn-outline-danger ms-2"
              onClick={() => {
                const confirmDelete = window.confirm(
                  "Are you sure you want to delete this CategoryPost?"
                );

                if (confirmDelete) {
                  deletePostHandler(post?.id);
                }
              }}
              disabled={isDeleteLoading}
            >
              <i className="fa fa-trash"></i>
            </button>
          </div>
        ),
      });
    });

    return posts;
  };

  if (isLoading) {
    return (
      <>
        <AdminLayout>
          <MetaData title={"All Category Posts"} />
          <Loader />
        </AdminLayout>
      </>
    );
  }

  return (
    <AdminLayout>
      <MetaData title={"All Category Posts"} />

      <h1 className="my-2 px-5">{data?.data?.length} Category Posts</h1>
      <div className="mb-2 px-5 content mt-5">
        <Link
          to="/admin/categoryPost/new"
          className="btn btn-outline-success me-2"
        >
          <i className="fa fa-plus"></i> Add Category Post
        </Link>
      </div>
      <MDBDataTable
        className="px-5 content"
        data={setPosts()}
        bordered
        striped
        hover
        responsive
      />
      <style jsx>{`
        .dataTables_wrapper .dataTables_length,
        .dataTables_wrapper .dataTables_filter,
        .dataTables_wrapper .dataTables_info,
        .dataTables_wrapper .dataTables_paginate {
          margin-bottom: 20px; /* Điều chỉnh giá trị theo ý muốn của bạn */
        }
      `}</style>
    </AdminLayout>
  );
};

export default ListCategoryPosts;
