import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { BASE_PATH } from "../../constants/constants";
import { clearCart } from "../features/cartSlice";
import { authApi } from "./authApi";

export const otpApi = createApi({
  reducerPath: "verificationOrderApi",
  baseQuery: fetchBaseQuery({
    baseUrl: BASE_PATH,
    prepareHeaders: (headers, { getState }) => {
      const token = localStorage.getItem("token");
      if (token) {
        headers.set("Authorization", `Bearer ${token}`);
      }
      headers.set("Access-Control-Allow-Origin", `*`);
      return headers;
    },
  }),
  endpoints: (builder) => ({
    verifyOrder: builder.mutation({
      query({ otp, orderId }) {
        return {
          url: `/verification-order/${otp}/${orderId}`,
          method: "GET",
        };
      },
      onQueryStarted: async (args,{dispatch, queryFulfilled}) => {
        await queryFulfilled;
        dispatch(authApi.util.invalidateTags(["Users", "Auth"]));
        dispatch(clearCart());
      }
    }),
    resendOTP: builder.mutation({
      query({ orderId }) {
        return {
          url: `/resent-otp/${orderId}`,
          method: "GET",
        };
      },
    }),
    verifyWithdraw: builder.mutation({
      query({ otp, withdrawId }) {
        return {
          url: `/request-withdraw-gold/verify/${otp}/${withdrawId}`,
          method: "POST",
        };
      },
    }),
    resendOTPWithdraw: builder.mutation({
      query({ withdrawId }) {
        return {
          url: `/resent-otp-withdraw/${withdrawId}`,
          method: "GET",
        };
      },
    }),
  }),
});

export const {
  useVerifyOrderMutation,
  useResendOTPMutation,
  useVerifyWithdrawMutation,
  useResendOTPWithdrawMutation,
} = otpApi;
