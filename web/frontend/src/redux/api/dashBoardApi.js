import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { BASE_PATH } from "../../constants/constants";

export const dashboardApi = createApi({
  reducerPath: "dashboardApi",
  baseQuery: fetchBaseQuery({
    baseUrl: BASE_PATH,
    prepareHeaders: (headers, { getState }) => {
      const token = localStorage.getItem("token");
      if (token) {
        headers.set("Authorization", `Bearer ${token}`);
      }
      headers.set("Access-Control-Allow-Origin", `*`);
      return headers;
    },
  }),
  tagTypes: ["Dashboard"],
  endpoints: (builder) => ({
    calculateOrderSales: builder.mutation({
      query({ type, monthChose }) {
        return {
          url: `/calculate-order-sales`,
          params: { type, monthChose },
          method: "GET",
        };
      },
      providesTags: ["Dashboard"],
    }),
    calculateTransactionBuySales: builder.mutation({
      query({ type, monthChose }) {
        return {
          url: `/calculate-transaction-buy-sales`,
          params: { type, monthChose },
          method: "GET",
        };
      },
      providesTags: ["Dashboard"],
    }),
    calculateTransactionSellSales: builder.mutation({
      query({ type, monthChose }) {
        return {
          url: `/calculate-transaction-sell-sales`,
          params: { type, monthChose },
          method: "GET",
        };
      },
      providesTags: ["Dashboard"],
    }),
    calculateWithdraw: builder.mutation({
      query({ type, monthChose }) {
        return {
          url: `/calculate-withdraw-request`,
          params: { type, monthChose },
          method: "GET",
        };
      },
      providesTags: ["Dashboard"],
    }),
    quantityStatistics: builder.mutation({
      query() {
        return {
          url: `/quantity-statistic`,
          method: "GET",
        };
      },
      providesTags: ["Dashboard"],
      transformResponse: (response) => {
        response.data.quantityProductReviews = [
          response.data.quantityReviewOneStar,
          response.data.quantityReviewTwoStar,
          response.data.quantityReviewThreeStar,
          response.data.quantityReviewFourStar,
          response.data.quantityReviewFiveStar,
        ];
        return response;
      },
    }),
  }),
});

export const {
  useCalculateOrderSalesMutation,
  useCalculateTransactionBuySalesMutation,
  useCalculateTransactionSellSalesMutation,
  useCalculateWithdrawMutation,
  useQuantityStatisticsMutation,
} = dashboardApi;
export const DashBoardDateType = Object.freeze({
  DAY: "DAY",
  WEEK: "WEEK",
  MONTH: "MONTH",
});
