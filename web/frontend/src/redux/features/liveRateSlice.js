import { createSlice } from "@reduxjs/toolkit";
import { format } from "date-fns";
import { isMarketOpen } from "../../helpers/helpers";

let liveRate = localStorage.getItem("live-rate");
if (liveRate) {
  try {
    liveRate = JSON.parse(liveRate);
  } catch (e) {
    liveRate = null;
  }
}
const isMarketClose = !isMarketOpen()
const initialState = {
  data: liveRate,
  isLoading: false,
  historyData: null,
  tradeData: null,
  marketClosed: isMarketClose,
};
const liveRateSlice = createSlice({
  name: "liveRateSlice",
  initialState,
  reducers: {
    setLiveRate: (state, actions) => {
      const newLiveRate = actions.payload ?? {};
      if (newLiveRate?.ts === state.data?.ts) return;
      // Compare the old live rate and set changedPrice and changedPercentage
      if (state.data) {
        const changedPrice = newLiveRate.mid - state.data.mid;
        const changedPercentage = changedPrice / state.data.mid;
        newLiveRate.changedPrice = changedPrice;
        newLiveRate.changedPercentage = changedPercentage;
      } else {
        newLiveRate.changedPrice = 0.0;
        newLiveRate.changedPercentage = 0.0;
      }
      newLiveRate.lastUpdated =
      newLiveRate.ts && format(new Date(Number(newLiveRate.ts)), "HH:mm:ss");
      // store final live Rate
      state.data = newLiveRate;
      localStorage.setItem("live-rate", JSON.stringify(newLiveRate));
      state.marketClosed = false;
      state.isLoading = false;
    },
    setIsLoading: (state, action) => {
      state.isLoading = action.payload;
    },
    setTradeData: (state, action) => {
      state.tradeData = action.payload;
    },
    setHistoryData: (state, action) => {
      state.historyData = action.payload;
    },
  },
});

export default liveRateSlice.reducer;

export const { setLiveRate, setTradeData, setIsLoading, setHistoryData } =
  liveRateSlice.actions;
